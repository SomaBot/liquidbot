package org.soma.miner.actions.powermine;

import org.liquidbot.bot.script.Action;
import org.liquidbot.bot.script.api.methods.data.Inventory;
import org.liquidbot.bot.script.api.util.Random;
import org.liquidbot.bot.script.api.util.Time;
import org.soma.miner.Miner;

public class Drop extends Action {

	@Override
	public boolean activate() {
		return Inventory.isFull();
	}

	@Override
	public void execute() {
			for (int x = 0; x < 29; x++) {
				Inventory.getItemAt(x).interact("Drop");
			Miner.status = "Dropping ore...";
			Time.sleep(Random.nextInt(750, 1000));
		}
	}
}
