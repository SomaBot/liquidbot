package org.soma.miner.actions.powermine;

import org.liquidbot.bot.script.Action;
import org.liquidbot.bot.script.api.methods.data.Inventory;
import org.liquidbot.bot.script.api.methods.data.movement.Camera;
import org.liquidbot.bot.script.api.methods.interactive.GameEntities;
import org.liquidbot.bot.script.api.methods.interactive.Players;
import org.liquidbot.bot.script.api.util.Random;
import org.liquidbot.bot.script.api.util.Time;
import org.liquidbot.bot.script.api.wrappers.GameObject;
import org.soma.miner.Miner;

public class Mine extends Action {

	@Override
	public boolean activate() {
		return !Inventory.isFull() && !Players.getLocal().isMoving();
	}

	@Override
	public void execute() {
		GameObject ore = GameEntities.getNearest(Miner.oreID);
		if (!ore.isOnScreen()) {
			Camera.turnTo(ore);
			Miner.status = "Looking for rock...";
		}
		if (ore.isOnScreen() && Players.getLocal().getAnimation() == -1) {
			ore.interact("Mine");
			{
				Time.sleep(Random.nextInt(1750, 2500));
				Miner.oreCount++;
			}
			Miner.status = "Mining ore...";
		}
	}
}
