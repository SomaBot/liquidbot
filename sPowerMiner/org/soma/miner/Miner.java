package org.soma.miner;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import javax.swing.SwingUtilities;

import org.liquidbot.bot.script.ActionScript;
import org.liquidbot.bot.script.Manifest;
import org.liquidbot.bot.script.api.interfaces.PaintListener;
import org.soma.miner.gui.GUI;
import org.soma.miner.actions.powermine.*;

@Manifest(name = "sMiner", author = "Soma", description = "Use this script to mine stuff")
public class Miner extends ActionScript implements PaintListener {
	public static String status;
	public static int[] oreID;
	public static int oreCount;
    long startTime = System.currentTimeMillis();

	@Override
	public void start() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new GUI().setVisible(true);
			}
		});
		provide(new Mine());
		provide(new Drop());
		System.out.println("Start time: " + startTime);
	}
	private String runTime() {
		final int sec = (int) (getRuntime() / 1000), h = sec / 3600, m = sec / 60 % 60, s = sec % 60;
		return (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m) + ":"
				+ (s < 10 ? "0" + s : s);
	}
	String tmp = String.valueOf(getRuntime());
	int oph = Integer.parseInt(tmp);
	int minedPH = (int) ((oreCount * 3600000D) / (oph));
	private final Color color1 = new Color(255, 255, 255);

	private final Font font1 = new Font("Arial", 1, 25);
	private final Font font2 = new Font("Arial", 1, 16);

	@Override
	public void render(Graphics2D g) {
		g.setFont(font1);
        g.setColor(color1);
        g.drawString("sMiner", 0, 50);
        g.setFont(font2);
        g.drawString("Runtime: " + runTime(), 0, 75);
        g.drawString("Mined: " + oreCount, 0, 100);
        g.drawString("Mined p/h: " + minedPH, 0, 125);
        g.drawString("Status: " + status, 0, 150);

	}

}
