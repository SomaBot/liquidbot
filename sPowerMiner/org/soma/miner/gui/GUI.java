package org.soma.miner.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.soma.miner.Miner;

public class GUI extends JFrame {
	public GUI() {
		initComponents();
	}

	private void button1ActionPerformed(ActionEvent e) {
	}

	private void initComponents() {
		JPanel content = new JPanel();
		add(content);
		textField1 = new JTextField(5);
		textField1.setToolTipText("Enter ID here");
		button1 = new JButton();
		
		// Main window
		setResizable(false);
		setSize(225, 90);
		validate();
		setLocationRelativeTo(null);
		content.add(textField1);
		content.add(button1);
		content.setLayout(new FlowLayout());
		

		// start button
		button1.setText("Start");
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				button1ActionPerformed(e);
				String[] strings = textField1.getText().split(", *");
				  Miner.oreID = new int[strings.length];
				  for(int i = 0; i < strings.length; i++) 
					  Miner.oreID[i] = Integer.parseInt(strings[i].trim());
				System.out.println("ID of ore: " + Miner.oreID);
				dispose();
			}
		});

	}

	public static JTextField textField1;
	private JButton button1;
}
