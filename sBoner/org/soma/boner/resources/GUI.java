package org.soma.boner.resources;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.soma.boner.sBoner;


public class GUI {
	public JFrame frame = new JFrame(); {
		initComponents();
	}

	private void button1ActionPerformed(ActionEvent e) {
	}

	private void initComponents() {
		JPanel content = new JPanel();
		frame.add(content);
		content.setLayout(new FlowLayout());
		textField = new JTextField(5);
		button1 = new JButton();

		// Main window
		frame.setResizable(false);
		frame.setSize(90, 68);
		frame.validate();
		frame.setLocationRelativeTo(null);
		content.add(textField);
		content.add(button1);
		textField.setToolTipText("Enter ID here");

		// start button
		button1.setText("Start");
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				button1ActionPerformed(e);
				sBoner.boneID = Integer.parseInt(textField.getText());
				frame.dispose();

			}
		});
	}

	private JTextField textField;
	private JButton button1;
}
