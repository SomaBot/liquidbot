package org.soma.boner.actions;

import org.liquidbot.bot.script.Action;
import org.liquidbot.bot.script.api.methods.data.Inventory;

import org.soma.boner.sBoner;


public class Bank extends Action {
	@Override
	public boolean activate() {
		return Inventory.isEmpty(); 
	}

	@Override
	public void execute() {
		if (!org.liquidbot.bot.script.api.methods.data.Bank.isOpen()) {
		org.liquidbot.bot.script.api.methods.data.Bank.open();			
		} else {
			org.liquidbot.bot.script.api.methods.data.Bank.depositInventory();
			org.liquidbot.bot.script.api.methods.data.Bank.withdraw(sBoner.boneID, 28);
			org.liquidbot.bot.script.api.methods.data.Bank.close();
		}
	}
}