package org.soma.boner.actions;

import org.liquidbot.bot.script.Action;
import org.liquidbot.bot.script.api.methods.data.Game;
import org.liquidbot.bot.script.api.methods.data.Inventory;
import org.liquidbot.bot.script.api.util.Time;
import org.soma.boner.sBoner;

public class Bury extends Action {
	@Override
	public boolean activate() {
		return Game.isLoggedIn() && Inventory.isFull();
	}

	@Override
	public void execute() {
		for (int x = 0; x < 29; x++) {
			Inventory.getItemAt(x).interact("Bury");
			{
				sBoner.burried++;
			}
			Time.sleep(600, 1000);
		}
	}
}