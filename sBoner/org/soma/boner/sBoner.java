package org.soma.boner;

import org.liquidbot.bot.script.ActionScript;
import org.liquidbot.bot.script.Manifest;
import org.liquidbot.bot.script.api.interfaces.PaintListener;
import org.soma.boner.actions.Bank;
import org.soma.boner.actions.Bury;
import org.soma.boner.resources.GUI;
import java.awt.*;

import javax.swing.SwingUtilities;

@Manifest(name = "sBoner", author = "Soma", description = "Burries any type of bone at any bank!")
public class sBoner extends ActionScript implements PaintListener {
	public static int boneID;
	public static int burried;
	@Override
	public void start() {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new GUI().frame.setVisible(true);
			}
		});
		provide(new Bury());
		provide(new Bank());
	}
	private String runTime() {
		final int sec = (int) (getRuntime() / 1000), h = sec / 3600, m = sec / 60 % 60, s = sec % 60;
		return (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m) + ":"
				+ (s < 10 ? "0" + s : s);
	}
	private final Color color1 = new Color(255, 255, 255);

	private final Font font1 = new Font("Arial", 1, 30);
	private final Font font2 = new Font("Arial", 1, 15);

	@Override
	public void render(Graphics2D graphics2D) {
		String tmp = String.valueOf(getRuntime());
		int bph = Integer.parseInt(tmp);
		int burriedPH = (int) ((burried * 3600000D) / (bph));
		graphics2D.setFont(font1);
		graphics2D.setColor(color1);
		graphics2D.drawString("sBoner", 0, 75);
		graphics2D.setFont(font2);
		graphics2D.drawString("Runtime: " + runTime(), 0, 100);
		graphics2D.drawString("Burried: " + burried, 0, 125);
		graphics2D.drawString("Burried p/h: " + burriedPH, 0, 150);

	}

}